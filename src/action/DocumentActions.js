import AppDispatcher from '../dispatcher/AppDispatcher';
import DocumentActionTypes from '../constant/DocumentActionTypes';

class DocumentActions {

  fetchTemplates(data, err) {
    AppDispatcher.handleServerAction({
      type: DocumentActionTypes.FETCH_TEMPLATES,
      data: data,
      error: err
    });
  }

  fetchDocuments(data, err) {
    AppDispatcher.handleServerAction({
      type: DocumentActionTypes.FETCH_DOCUMENTS,
      data: data,
      error: err
    });
  }

  fetchDocumentsBox(data, err) {
    AppDispatcher.handleServerAction({
      type: DocumentActionTypes.FETCH_DOCUMENTS_BOX,
      data: data,
      error: err
    });
  }

  documentDispatched(data, err) {
    AppDispatcher.handleServerAction({
      type: DocumentActionTypes.DOCUMENT_DISPATCHED,
      data: data,
      error: err
    });
  }

}

export default DocumentActions;
