import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthenticationActionTypes from '../constant/AuthenticationActionTypes';

class AuthenticationActions {

  authenticate(data, err) {
    AppDispatcher.handleServerAction({
      type: AuthenticationActionTypes.AUTHENTICATE,
      data: data,
      error: err
    });
  }

  logout() {
    AppDispatcher.handleServerAction({
      type: AuthenticationActionTypes.LOGOUT
    });
  }

}

export default AuthenticationActions;
