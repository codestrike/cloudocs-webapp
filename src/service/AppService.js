const SERVICE_API_URL = "http://40.71.192.54:9000/api-cloudocs/api/";
// const SERVICE_API_URL = "http://172.18.0.155:9000/api-cloudocs/api/";
const AUTHENTICATION_API_URL = "http://40.71.192.54:9000/api-cloudocs/auth";

class AppService {

  constructor() {
    this.serviceApiUrl = SERVICE_API_URL;
    this.authApiUrl = AUTHENTICATION_API_URL;
  }

}

export default AppService;
