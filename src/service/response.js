var response = {
  id: "59febbfbd5f36f2470000e72",
  type: "Administrativo",
  code: null,
  state: null,
  dependenceCode: "d001",
  dependence: "Sub Gerencia",
  sender: {
    code: "00001",
    type: "Administrador",
    name: "William Martinez"
  },
  fileName: "/home/javofegus/Storage/59febbfbd5f36f2470000e72/Memorando.pdf",
  templateBean: {
    id: "59fe6fa75869ee03bddd5e4f",
    attributes:[
      {
        name: "receiver",
        label: "Receptor",
        value: "Javier",
        type: "STRING"
      },
      {
        name: "issuing",
        label: "Emisor",
        value: "William",
        type: "STRING"
      },
      {
        name: "date",
        label: "Fecha",
        value: "15/05/2018",
        type: "STRING"
      },
      {
        name: "subject",
        label: "Subject",
        value: "Memorandum por tardanzas",
        type: "TEXT"
      },
      {
        name: "detail",
        label: "Detalle",
        value: "Lorem impfkdjgk",
        type: "TEXT"
      }
    ],
    url: "/home/javofegus/Documents/templates/Memorandum.docx",
    name: "Memorando"
  },
  subject: "Memorandum - Tardanza",
  fecha: 1509866491810
};

export default response;
