import request from 'reqwest';
import AppService from './AppService';
import AuthenticationActions from '../action/AuthenticationActions';
import ErrorConstants from './ErrorConstants';

class AuthenticationService extends AppService {

  authenticate(params) {
    let authActions = new AuthenticationActions();
    request({
      url: this.authApiUrl,
      method: 'POST',
      crossOrigin: true,
      data: JSON.stringify(params),
      dataType: "JSON",
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      if (typeof response.readyState === 'undefined') {
        authActions.authenticate(response, null);
      } else {
        authActions.authenticate(null, { code: ErrorConstants.INVALID_CREDENTIALS });
      }
    })
    .catch((err) => {
      authActions.authenticate(null, { code: ErrorConstants.TIMEOUT });
    });
  }

  logout(authToken) {
    let authActions = new AuthenticationActions();
    request({
      url: `${this.authApiUrl}/logout`,
      method: 'POST',
      crossOrigin: true,
      data: {
        authToken: authToken
      }
    })
    .then(response => {
      authActions.logout();
    })
  }

}

export default new AuthenticationService();
