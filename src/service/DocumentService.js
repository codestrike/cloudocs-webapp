import request from 'reqwest';
import AppService from './AppService';
import DocumentActions from '../action/DocumentActions';
import ErrorConstants from './ErrorConstants';

class DocumentService extends AppService {

  getTemplates() {
    let documentActions = new DocumentActions();
    request({
      url: `${this.serviceApiUrl}templates`,
      method: 'GET',
      crossOrigin: true,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      documentActions.fetchTemplates(response, {});
    })
    .catch((err) => {
      if (err.readyState === 4 && err.status === 0) {
        documentActions.fetchTemplates([], { code: ErrorConstants.TIMEOUT });
      }
    });
  }

  getDocuments(params) {
    request({
      url: `${this.serviceApiUrl}documents?id=${params.user.id}`,
      method: 'GET',
      crossOrigin: true,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      new DocumentActions().fetchDocuments(response, {});
    })
    .catch((err) => {
      if (err.readyState === 4 && err.status === 0) {
        new DocumentActions().fetchDocuments([], { code: ErrorConstants.TIMEOUT });
      }
    });
  }

  getDocumentsBox(params) {
    request({
      url: `${this.serviceApiUrl}destinations?id=${params.user.id}`,
      method: 'GET',
      crossOrigin: true,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      new DocumentActions().fetchDocumentsBox(response, {});
    })
    .catch((err) => {
      if (err.readyState === 4 && err.status === 0) {
        new DocumentActions().fetchDocumentsBox([], { code: ErrorConstants.TIMEOUT });
      }
    });
  }

  dispatchDocument(params) {

    let req = {
      document: {
        type: "Administrativo",
        dependenceCode: "d001",
        dependence: params.user.dependency,
        sender: {
          code: params.user.id,
          type: params.user.role.description,
          name: `${params.user.name} ${params.user.lastName}`
        },
        template: params.template,
        subject: params.subject
      },
      destinationCode: params.destinationCode
    }

    request({
      url: `${this.serviceApiUrl}document`,
      method: 'POST',
      crossOrigin: true,
      data: JSON.stringify(req),
      dataType: 'JSON',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      new DocumentActions().documentDispatched(response, null);
    })
    .catch((err) => {
      alert(err);
      // if (err.readyState === 4 && err.status === 0) {
      //   documentActions.fetchTemplates([], { code: ErrorConstants.TIMEOUT });
      // }
    });
  }

}

export default new DocumentService();
