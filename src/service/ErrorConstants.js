import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({

  TIMEOUT: null,
  INVALID_CREDENTIALS: null

});
