import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  FETCH_TEMPLATES: null,
  DOCUMENT_DISPATCHED: null,
  FETCH_DOCUMENTS: null,
  FETCH_DOCUMENTS_BOX: null
});
