import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({

  VIEW_ACTION: null,
  SERVER_ACTION: null

});
