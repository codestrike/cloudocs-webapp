import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  AUTHENTICATE: null,
  LOGOUT: null
});
