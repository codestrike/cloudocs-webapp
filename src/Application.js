import React, { Component } from 'react';
import Navbar from './component/common/Navbar';
import AuthenticationStore from './store/AuthenticationStore';
import { browserHistory } from 'react-router';
import request from 'reqwest';
import './stylesheet/Application.css';

class Application extends Component {

  componentWillMount() {

    let subdomain = window.location.hostname.split(".")[0];
    if (subdomain !== "public") {
      request({
        url: `http://localhost:8080/api-cloudocs/organizations?slug=${subdomain}`,
        method: 'GET',
        crossOrigin: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        if (typeof response.readyState !== 'undefined') {
          // window.location.href = "http://public.cloudocs.pe:3000";
        } else {
          localStorage.setItem("organization", JSON.stringify(response));
        }
      });
    }

    if (!AuthenticationStore.isAuthenticated()) {
      browserHistory.push("/login");
    }
  }

  render() {
    return (
      <div className="Application">
        {
          (this.props.location.pathname !== "/login" && this.props.routes[1].path !== "*") ?
            <Navbar />
          :
            null
        }
        <div className="container">
          {this.props.children}
        </div>
      </div>
    )
  }

}

export default Application
