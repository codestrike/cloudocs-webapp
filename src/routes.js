import Application from './Application';
import Home from './component/Home';
import Login from './component/Login';
import Profile from './component/Profile';
import Documents from './component/Documents';
import CreateDocumentForm from './component/documents/CreateDocumentForm';
import DocumentsBox from './component/documents/DocumentsBox';
import NotFound from './component/NotFound';

const routes = {
  path: '/',
  component: Application,
  indexRoute: { component: Home, name: 'home' },
  childRoutes: [
    { path: 'login', component: Login },
    { path: 'profile', component: Profile },
    { path: 'documents', component: Documents },
    { path: 'documents/new', component: CreateDocumentForm },
    { path: 'documents/inbox', component: DocumentsBox },
    { path: '*', component: NotFound }
  ]
}

export default routes;
