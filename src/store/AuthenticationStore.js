import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthenticationActionTypes from '../constant/AuthenticationActionTypes';
import EventEmitter from 'wolfy87-eventemitter';
import assign from 'object-assign';
import ErrorConstants from '../service/ErrorConstants';

var _user = null;
var _authToken = null;
var _error = null;

const AUTHENTICATE = 'AUTHENTICATE';
const LOGOUT = 'LOGOUT';

function setUser(data) {
  _user = data;
  localStorage.setItem("user", JSON.stringify(_user));
}

function setAuthToken(data) {
  _authToken = data.authToken;
  localStorage.setItem("authToken", _authToken);
}

function setError(err) {
  var message = "";
  switch (err.code) {
    case ErrorConstants.INVALID_CREDENTIALS:
      message = "Tus credenciales son incorrectas";
      break;
    default:
  }

  _error = { message: message };
}

function removeUser() {
  localStorage.removeItem("user");
}

function removeAuthToken() {
  localStorage.removeItem("authToken");
}

var AuthenticationStore = assign({}, EventEmitter.prototype, {

  getUser() {
    if (localStorage.user) {
      _user = JSON.parse(localStorage.user);
    }
    return _user;
  },

  getAuthToken() {
    if (localStorage.authToken) {
      _authToken = localStorage.authToken;
    }
    return _authToken;
  },

  isAuthenticated() {
    return localStorage.authToken;
  },

  getError() {
    return _error;
  },

  emitAuthenticate() {
    this.emit(AUTHENTICATE);
  },

  addAuthenticateListener(callback) {
    this.on(AUTHENTICATE, callback);
  },

  removeAuthenticateListener(callback) {
    this.off(AUTHENTICATE, callback);
  },

  emitLogout() {
    this.emit(LOGOUT);
  },

  addLogoutListener(callback) {
    this.on(LOGOUT, callback);
  },

  removeLogoutListener(callback) {
    this.off(LOGOUT, callback);
  }

});

AuthenticationStore.dispatcherToken = AppDispatcher.register((payload) => {

  var action = payload.action;

  switch (action.type) {
    case AuthenticationActionTypes.AUTHENTICATE:
      if (action.data !== null) {
        setUser(action.data);
        setAuthToken(action.data);
      } else {
        setError(action.error);
      }
      AuthenticationStore.emitAuthenticate();
      break;

    case AuthenticationActionTypes.LOGOUT:
      removeUser();
      removeAuthToken();
      AuthenticationStore.emitLogout();
      break;

    default:
      // do nothing

  }

});

export default AuthenticationStore;
