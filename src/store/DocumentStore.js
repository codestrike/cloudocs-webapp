import AppDispatcher from '../dispatcher/AppDispatcher';
import DocumentActionTypes from '../constant/DocumentActionTypes';
import EventEmitter from 'wolfy87-eventemitter';
import assign from 'object-assign';
import ErrorConstants from '../service/ErrorConstants';

var _templates = [];
var _documents = [];
var _documentsBox = [];
var _error = null;

const FETCH_TEMPLATES = 'FETCH_TEMPLATES';
const DOCUMENT_DISPATCHED = 'DOCUMENT_DISPATCHED';
const FETCH_DOCUMENTS = 'FETCH_DOCUMENTS';
const FETCH_DOCUMENTS_BOX = 'FETCH_DOCUMENTS_BOX';

function setTemplates(data) {
  _templates = data;
}

function setDocuments(data) {
  _documents = data;
}

function setDocumentsBox(data) {
  _documentsBox = data;
}

function setError(err) {
  var message = "";
  switch (err.code) {
    case ErrorConstants.TIMEOUT:
      message = "Ha ocurrido un problema con nuestros servidores. Vuelva a intentarlo más tarde.";
      break;
    default:
  }

  _error = { message: message };
}

var DocumentStore = assign({}, EventEmitter.prototype, {

  getTemplates() {
    return _templates;
  },

  getDocuments() {
    return _documents;
  },

  getDocumentsBox() {
    return _documentsBox;
  },

  getError() {
    return _error;
  },

  emitFetchTemplates() {
    this.emit(FETCH_TEMPLATES);
  },

  addFetchTemplatesListener(callback) {
    this.on(FETCH_TEMPLATES, callback);
  },

  removeFetchTemplatesListener(callback) {
    this.off(FETCH_TEMPLATES, callback);
  },

  emitFetchDocuments() {
    this.emit(FETCH_DOCUMENTS);
  },

  addFetchDocumentsListener(callback) {
    this.on(FETCH_DOCUMENTS, callback);
  },

  removeFetchDocumentsListener(callback) {
    this.off(FETCH_DOCUMENTS, callback);
  },

  emitFetchDocumentsBox() {
    this.emit(FETCH_DOCUMENTS_BOX);
  },

  addFetchDocumentsBoxListener(callback) {
    this.on(FETCH_DOCUMENTS_BOX, callback);
  },

  removeFetchDocumentsBoxListener(callback) {
    this.off(FETCH_DOCUMENTS_BOX, callback);
  },

  emitDocumentDispatched() {
    this.emit(DOCUMENT_DISPATCHED);
  },

  addDocumentDispatchedListener(callback) {
    this.on(DOCUMENT_DISPATCHED, callback);
  },

  removeDocumentDispatchedListener(callback) {
    this.off(DOCUMENT_DISPATCHED, callback);
  },


});

DocumentStore.dispatcherToken = AppDispatcher.register((payload) => {

  var action = payload.action;

  switch (action.type) {
    case DocumentActionTypes.FETCH_TEMPLATES:
      if (action.data.length > 0) {
        setTemplates(action.data);
      } else {
        setError(action.error);
      }
      DocumentStore.emitFetchTemplates();
      break;

    case DocumentActionTypes.FETCH_DOCUMENTS:
      if (action.data.length > 0) {
        setDocuments(action.data);
      } else {
        setError(action.error);
      }
      DocumentStore.emitFetchDocuments();
      break;

    case DocumentActionTypes.FETCH_DOCUMENTS_BOX:
      setDocumentsBox(action.data);
      DocumentStore.emitFetchDocumentsBox();
      break;

    case DocumentActionTypes.DOCUMENT_DISPATCHED:
      if (action.data !== null) {
        DocumentStore.emitDocumentDispatched();
      }
      break;

    default:
      // do nothing

  }

});

export default DocumentStore;
