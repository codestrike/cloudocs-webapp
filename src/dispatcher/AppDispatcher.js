import Flux from 'flux';
import PayloadSources from '../constant/PayloadSources';
import assign from 'object-assign';

let AppDispatcher = assign(new Flux.Dispatcher(), {

  handleServerAction(action) {
    var payload = {
      source: PayloadSources.SERVER_ACTION,
      action: action
    };
    this.dispatch(payload);
  },

  handleViewAction(action) {
    var payload = {
      source: PayloadSources.VIEW_ACTION,
      action: action
    };
    this.dispatch(payload);
  }

});

export default AppDispatcher;
