import React, { Component } from 'react';
import AuthenticationService from '../service/AuthenticationService';
import AuthenticationStore from '../store/AuthenticationStore';
import { browserHistory } from 'react-router';

class Login extends Component {
  constructor() {
    super();

    this.state = {
      email: "",
      password: "",
      error: null,
      organization: null
    };

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onAuthenticate = this.onAuthenticate.bind(this);
  }

  componentWillMount() {
    let organization = JSON.parse(localStorage.getItem("organization"));
    this.setState({
      organization: organization
    })

    if (AuthenticationStore.isAuthenticated()) {
      browserHistory.push("/");
    }
  }

  componentDidMount() {
    AuthenticationStore.addAuthenticateListener(this.onAuthenticate);
  }

  componentWillUnmount() {
    AuthenticationStore.removeAuthenticateListener(this.onAuthenticate);
  }

  onAuthenticate() {
    let error = AuthenticationStore.getError();

    if (error === null) {
      browserHistory.push("/");
    } else {
      this.setState({
        error: error
      });
    }
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    let params = {
      email: this.state.email,
      password: this.state.password
    };

    AuthenticationService.authenticate(params);
  }

  render() {
    return (
      <div className="Login">
        <div className="row">
          <div className="col-md-4 col-md-offset-4">
            <div className="panel" style={{textAlign: 'center'}}>
              <h3>ClouDocs</h3>
              <p className="helper-text">Ingrese sus credenciales</p>


              {
                this.state.error !== null ?
                  <div className="alert alert-danger">
                    {this.state.error.message}
                  </div>
                :
                  null
              }

              <form action="/auth" onSubmit={this.onSubmit}>
                <div className="row" style={{marginBottom: '15px'}}>
                  <div className="col-md-12">
                    <input type="text" className="form-control" onChange={this.onChangeEmail} placeholder="Correo electrónico" />
                  </div>
                </div>
                <div className="row" style={{marginBottom: '15px'}}>
                  <div className="col-md-12">
                    <input type="password" className="form-control" onChange={this.onChangePassword} placeholder="Contraseña" />
                  </div>
                </div>

                <div className="Login__actions">
                  <button type="submit" className="btn btn-primary">Iniciar sesión</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login;
