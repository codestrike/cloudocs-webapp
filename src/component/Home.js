import React, { Component } from 'react';
import AuthenticationStore from '../store/AuthenticationStore';

class Home extends Component {
  constructor() {
    super();

    this.state = {
      user: AuthenticationStore.getUser()
    }
  }

  render() {
    let span = this.state.user !== null ?
      <span>{this.state.user.name} {this.state.user.lastName}</span>
    :
      null

    return (
      <div className="Home">
        <div className="panel">
          <h1>Welcome! {span}</h1>
        </div>
      </div>
    )
  }
}

export default Home;
