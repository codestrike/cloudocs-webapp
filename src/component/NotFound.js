import React, { Component } from 'react';
import '../stylesheet/NotFound.css';

class NotFound extends Component {
  render() {
    return (
      <div className="NotFound">
        <h1>404</h1>
        <p className="NotFound__text" style={{marginBottom: '30px'}}>Página no encontrada</p>

        <a href="/" className="btn btn-primary">Ir a página principal</a>
      </div>
    )
  }
}

export default NotFound;
