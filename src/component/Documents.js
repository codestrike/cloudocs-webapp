import React, { Component } from 'react';
import DocumentList from './documents/DocumentList';
import AuthenticationStore from '../store/AuthenticationStore';
import DocumentStore from '../store/DocumentStore';
import DocumentService from '../service/DocumentService';
import '../stylesheet/Documents.css';

class Documents extends Component {
  constructor() {
    super();

    this.state = {
      user: AuthenticationStore.getUser(),
      documents: []
    };

    this.onFetchDocuments = this.onFetchDocuments.bind(this);
  }

  componentDidMount() {
    DocumentStore.addFetchDocumentsListener(this.onFetchDocuments);

    DocumentService.getDocuments({user: this.state.user});
  }

  componentWillUnmount() {
    DocumentStore.removeFetchDocumentsListener(this.onFetchDocuments);
  }

  onFetchDocuments() {
    let documents = DocumentStore.getDocuments();
    if (documents.length > 0) {
      this.setState({
        documents: documents
      });
    }
  }

  render() {

    return (
      <div className="Documents">
        <div className="row">
          <div className="col-md-12">
            <div className="row">
              <div className="col-md-12">
                <h1>Documentos</h1>
              </div>
            </div>
            <div className="row">
              {
                this.state.user !== null ?
                  <div className="col-md-3">
                    <ul className="DocumentsType__list">
                      <li className="active"><a href="/documents?filter=all">Todos</a></li>
                      <li><a href="/documents?">Para despacho</a></li>
                      <li><a href="/documents?">En proyecto</a></li>
                      <li><a href="/documents?">Muy urgente</a></li>
                      <li><a href="/documents?">Urgente</a></li>
                      <li><a href="/documents?">No leído</a></li>
                    </ul>
                  </div>
                :
                  null
              }
              <div className={`${this.state.user !== null ? 'col-md-9' : 'col-md-12'}`}>
                <div className="panel">
                  <DocumentList documents={this.state.documents} isDocumentsBox={false}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Documents;
