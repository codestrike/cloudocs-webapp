import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import AuthenticationService from '../../service/AuthenticationService';
import AuthenticationStore from '../../store/AuthenticationStore';
import expandArrowIcon from '../../icons/expand-arrow.png';
import '../../stylesheet/Navbar.css';

class Navbar extends Component {
  constructor() {
    super();

    this.state = {
      user: AuthenticationStore.getUser(),
      isMenuExpanded: false
    };

    this.onToggleUserMenu = this.onToggleUserMenu.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.onSuccessLogout = this.onSuccessLogout.bind(this);
  }

  componentDidMount() {
    AuthenticationStore.addLogoutListener(this.onSuccessLogout);
  }

  componentWillUnmount() {
    AuthenticationStore.removeLogoutListener(this.onSuccessLogout);
  }

  onToggleUserMenu(e) {
    e.preventDefault();

    this.setState(prevState => ({
      isMenuExpanded: !prevState.isMenuExpanded
    }))
  }

  onLogout(e) {
    e.preventDefault();

    AuthenticationService.logout(AuthenticationStore.getAuthToken());
  }

  onSuccessLogout() {
    browserHistory.push("/login");
  }

  render() {

    return (
      <div className="Navbar navbar navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed"
              data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="/">ClouDocs</a>
          </div>
          <div id="navbar" className="collapse navbar-collapse">
            <ul className="nav navbar-nav">
              <li className="active"><a href="/">Inicio</a></li>
              <li><a href="/documents">Documentos</a></li>
              <li><a href="/documents/inbox">Bandeja de entrada</a></li>
            </ul>
            {
              this.state.user !== null ?
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <a href="/profile" onClick={this.onToggleUserMenu}>
                      {this.state.user.name}
                      <img src={expandArrowIcon} alt="Expand icon" className="arrowIcon" />
                    </a>
                    {
                      this.state.isMenuExpanded ?
                        <div className="Navbar__userMenu panel">
                          <ul>
                            <li><a href="/profile">Mi Perfil</a></li>
                            <li><a href="/" onClick={this.onLogout}>Cerrar sesión</a></li>
                          </ul>
                        </div>
                      :
                        null
                    }
                  </li>
                </ul>
              :
                null
            }
          </div>
        </div>
      </div>
    )
  }
}

export default Navbar;
