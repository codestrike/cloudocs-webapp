import React, { Component } from 'react';
import documentIcon from '../../icons/newdoc.png';

class TemplateDataForm extends Component {
  constructor() {
    super();

    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    let data = {
      name: event.target.name,
      value: event.target.value
    }

    if (event.target.value.trim !== "") {
      this.props.onFillAttribute.call(null, data);
    }
  }

  render() {


    return (
      <div className="TemplateDataForm">
        <form className="form-horizontal">
          <div className="col-md-12" style={{marginBottom: '15px'}}>
            <h4><img src={documentIcon} alt="Doc icon" /> {this.props.template.name}</h4>
            <p className="helper-text">Ingrese los campos solicitados para poder generar el documento en base a la plantilla elegida.</p>
          </div>
          <div className="col-md-12">
            {
              this.props.template.attributes.map((attr, i) => {
                let input;
                if (attr.type === "STRING") {
                  input = <input type="text" className="form-control" name={attr.name} onChange={this.onChange} />
                } else if (attr.type === "TEXT") {
                  input = <textarea style={{resize: 'none'}} className="form-control" rows="4" name={attr.name} onChange={this.onChange}></textarea>
                }

                return (
                  <div className="form-group" key={i}>
                    <label className="control-label col-md-4">{attr.label}</label>
                    <div className="col-md-6">
                      {input}
                    </div>
                  </div>
                )
              })
            }
          </div>
        </form>
      </div>
    )
  }
}

export default TemplateDataForm;
