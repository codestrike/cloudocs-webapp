import React, { Component } from 'react';
import moment from 'moment';
import emptyMailBoxIcon from '../../icons/empty.png';
import emptyMailDestinationBoxIcon from '../../icons/empty-box.png';

class DocumentList extends Component {
  render() {
    return (
      <div className="DocumentList">
        {
          this.props.documents.length > 0 ?
            <div className="row">
              {
                !this.props.isDocumentsBox ?
                  <div className="col-md-12" style={{marginBottom: '15px'}}>
                    <a href="/documents/new" className="btn btn-primary btn-bordered">Crear documento</a>
                  </div>
                :
                  null
              }
              <div className="col-md-12">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Asunto</th>
                      <th>Tipo</th>
                      <th>Estado</th>
                      <th>Fecha</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.documents.map((document) => {

                        let date;
                        let href = `http://40.71.192.54/${document.id}/`;

                        if (this.props.isDocumentsBox) {
                          date = moment(document.document.fecha);
                          href += document.document.template.name + ".pdf";
                        } else {
                          date = moment(document.fecha);
                          href += document.templateBean.name + ".pdf";
                        }

                        return (
                          <tr key={document.id}>
                            <td>{this.props.isDocumentsBox ? document.document.subject : document.subject}</td>
                            <th>{this.props.isDocumentsBox ? document.document.type : document.type}</th>
                            <td><span className="label label-info">Recibido</span></td>
                            <td>{date.format("DD/MM/YYYY HH:mm a")}</td>
                            <td><a target="_blank" href={href}>Ver documento</a></td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          :
            (this.props.isDocumentsBox) ?
              <div className="DocumentList--empty">
                <img src={emptyMailDestinationBoxIcon} alt="emtpy email icon" />
                <h4>No hay nada aquí</h4>
                <p className="DocumentList--empty__text">No tienes ningún documento en tu bandeja de entrada. Te notificaremos cuando te llegue alguno.</p>
              </div>
            :
              <div className="DocumentList--empty">
                <img src={emptyMailBoxIcon} alt="emtpy email icon" />
                <h4>Envía tu primer documento</h4>
                <p className="DocumentList--empty__text">Aún no has creado ningún documento ni derivado a ningún destinatario. Prueba creando un nuevo documento.</p>
                <a href="/documents/new" className="btn btn-primary">Crear documento</a>
              </div>
        }
      </div>
    )
  }
}

export default DocumentList;
