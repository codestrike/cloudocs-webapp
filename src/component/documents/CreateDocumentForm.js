import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import TemplateList from './TemplateList';
import TemplateDataForm from './TemplateDataForm';
import DocumentService from '../../service/DocumentService';
import DocumentStore from '../../store/DocumentStore';
import AuthenticationStore from '../../store/AuthenticationStore';
import DocumentForm from './DocumentForm';
import newDocumentIcon from '../../icons/document.png';
import editDocumentDisabledIcon from '../../icons/edit-disabled.png';
import editDocumentIcon from '../../icons/edit.png';
import sendDocumentDisabledIcon from '../../icons/send-disabled.png';
import sendDocumentIcon from '../../icons/send.png';
import '../../stylesheet/CreateDocumentForm.css';

class CreateDocumentForm extends Component {
  constructor() {
    super();
    this.state = {
      stepIndex: 0,
      wizardSteps: [
        {
          title: "Elija una plantilla",
          icon: newDocumentIcon,
          disabledIcon: newDocumentIcon,
          done: false,
          current: true
        },
        {
          title: "Edite el documento",
          icon: editDocumentIcon,
          disabledIcon: editDocumentDisabledIcon,
          done: false,
          current: false
        },
        {
          title: "Envíe el documento",
          icon: sendDocumentIcon,
          disabledIcon: sendDocumentDisabledIcon,
          done: false,
          current: false
        }
      ],
      templates: [],
      selectedTemplate: null,
      dispatchDocumentForm: [
        { name: "subject", value: null },
        { name: "destinationCode", value: null }
      ],
      isSubmitting: false
    };

    this.onSelectTemplate = this.onSelectTemplate.bind(this);
    this.onNext = this.onNext.bind(this);
    this.onFillAttribute = this.onFillAttribute.bind(this);
    this.onFetchTemplates = this.onFetchTemplates.bind(this);
    this.onFillForm = this.onFillForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onDocumentDispatched = this.onDocumentDispatched.bind(this);
  }

  componentDidMount() {
    DocumentStore.addFetchTemplatesListener(this.onFetchTemplates);
    DocumentStore.addDocumentDispatchedListener(this.onDocumentDispatched);

    DocumentService.getTemplates();
  }

  componentWillMount() {
    DocumentStore.removeFetchTemplatesListener(this.onFetchTemplates);
    DocumentStore.removeDocumentDispatchedListener(this.onDocumentDispatched);
  }

  onDocumentDispatched() {
    console.log("foo");
    browserHistory.push("/documents?success");
  }

  onFetchTemplates() {
    let templates = DocumentStore.getTemplates();
    this.setState({
      templates: templates
    })
  }

  onSelectTemplate(id) {
    var template;
    for (let t of this.state.templates) {
      if (t.id === id) {
        template = t;
      }
    };

    this.setState({
      selectedTemplate: template
    });
  }

  onNext() {
    if (this.state.stepIndex < 2) {
      var wizardSteps = this.state.wizardSteps;
      wizardSteps[this.state.stepIndex].done = true;
      wizardSteps[this.state.stepIndex].current = false;
      wizardSteps[this.state.stepIndex + 1].current = true;

      this.setState(prevState => ({
        stepIndex: prevState.stepIndex + 1,
        wizardSteps: wizardSteps
      }));
    }
  }

  onFillAttribute(data) {
    var isValid = true;

    for (let i in this.state.selectedTemplate.attributes) {
      let attr = this.state.selectedTemplate.attributes[i];

      if (attr.name === data.name) {
        let templateAttributes = this.state.selectedTemplate.attributes;
        templateAttributes[i].value = data.value;

        this.setState({
          templateAttributes: templateAttributes
        });
      }

      if (attr.value === null || attr.value === "") {
        isValid = false;
      }
    }

    this.setState({
      isTemplateDataFormValid: isValid
    });
  }

  onFillForm(data) {
    var isValid = true;
    var dispatchDocumentForm = [];

    for (let attr of this.state.dispatchDocumentForm) {
      if (attr.name === data.name) {
        attr.value = data.value;
      }

      if (attr.value === null || attr.value === "") {
        isValid = false;
      }

      dispatchDocumentForm.push(attr);
    }

    this.setState({
      isDispatchDocumentFormValid: isValid
    });
  }

  onSubmit() {
    let params = {
      template: this.state.selectedTemplate,
      subject: this.state.dispatchDocumentForm[0].value,
      destinationCode: this.state.dispatchDocumentForm[1].value,
      user: AuthenticationStore.getUser()
    };

    // console.log(params);

    DocumentService.dispatchDocument(params);

    this.setState({
      isSubmitting: true
    });
  }

  render() {
    const wizardSteps = this.state.wizardSteps.map((step, i) => {
      var className = "CreateDocumentForm__wizardStep col-md-4 no-padding ";
      className += `${step.current ? 'current' : ''} ${step.done ? 'done' : 'disabled'} ${i === 0 ? 'first' : ''} ${i === 2 ? 'last' : ''}`;
      return (
        <div className={className} key={i}>
          <div className="stepLine"></div>
          <div className="step">
            <img src={step.done || step.current ? step.icon : step.disabledIcon} alt="Icon" />
          </div>
          <div className="stepText">{step.title}</div>
        </div>
      )
    });


    return (
      <div className="CreateDocumentForm">
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            <h1>Nuevo documento</h1>
            <p className="helper-text">Crea un nuevo documento en base a una plantilla, edita el documento y envíelo a otros usuarios.</p>

            <div className="panel">
              <div className="row">
                <div className="col-md-12">
                  <div className="CreateDocumentForm__wizard">
                    {wizardSteps}
                  </div>

                  <div className="CreateDocumentForm__content">
                    <div className="row">
                      {
                        this.state.stepIndex === 0 ?
                          <TemplateList
                            templates={this.state.templates}
                            selectedTemplate={this.state.selectedTemplate}
                            onSelectTemplate={this.onSelectTemplate} />
                        :
                          null
                      }
                      {
                        this.state.stepIndex === 1 ?
                          <TemplateDataForm
                            ref="templateDataForm"
                            template={this.state.selectedTemplate}
                            onFillAttribute={this.onFillAttribute} />
                        :
                          null
                      }
                      {
                        this.state.stepIndex === 2 ?
                          <DocumentForm
                            template={this.state.selectedTemplate}
                            onFillForm={this.onFillForm} />
                        :
                          null
                      }
                    </div>
                  </div>

                  <div className="CreateDocumentForm__actions">
                    {
                      this.state.stepIndex === 0 ?
                        (this.state.selectedTemplate !== null) ?
                          <button className="btn btn-primary" onClick={this.onNext}>Siguiente</button>
                        :
                          <button className="btn disabled" disabled="disabled">Siguiente</button>
                      :
                        null
                    }
                    {
                      this.state.stepIndex === 1 ?
                        (this.state.isTemplateDataFormValid) ?
                          <button className="btn btn-primary" onClick={this.onNext}>Siguiente</button>
                        :
                          <button className="btn disabled" disabled="disabled">Siguiente</button>
                      :
                        null
                    }
                    {
                      this.state.stepIndex === 2 ?
                        (this.state.isDispatchDocumentFormValid) ?
                          (this.state.isSubmitting) ?
                            <button className="btn disabled" disabled="disabled">Enviando...</button>
                          :
                            <button className="btn btn-success" onClick={this.onSubmit}>Enviar documento</button>
                        :
                          <button className="btn disabled" disabled="disabled">Enviar</button>
                      :
                        null
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CreateDocumentForm;
