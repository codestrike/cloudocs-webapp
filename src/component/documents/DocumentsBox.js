import React, { Component } from 'react';
import DocumentList from '../documents/DocumentList';
import AuthenticationStore from '../../store/AuthenticationStore';
import DocumentStore from '../../store/DocumentStore';
import DocumentService from '../../service/DocumentService';
import '../../stylesheet/Documents.css';

class DocumentsBox extends Component {
  constructor() {
    super();

    this.state = {
      user: AuthenticationStore.getUser(),
      documents: []
    };

    this.onFetchDocumentsBox = this.onFetchDocumentsBox.bind(this);
  }

  componentDidMount() {
    DocumentStore.addFetchDocumentsBoxListener(this.onFetchDocumentsBox);

    DocumentService.getDocumentsBox({user: this.state.user});
  }

  componentWillUnmount() {
    DocumentStore.removeFetchDocumentsBoxListener(this.onFetchDocumentsBox);
  }

  onFetchDocumentsBox() {
    let documents = DocumentStore.getDocumentsBox();
      console.log(documents);
    if (documents.length > 0) {
      this.setState({
        documents: documents
      });
    }
  }

  render() {

    return (
      <div className="Documents">
        <div className="row">
          <div className="col-md-12">
            <div className="row">
              <div className="col-md-12">
                <h1>Documentos</h1>
              </div>
            </div>
            <div className="row">
              {
                this.state.user !== null ?
                  <div className="col-md-3">
                    <ul className="DocumentsType__list">
                      <li className="active"><a href="/documents/inbox?filter=all">Todos</a></li>
                      <li><a href="/documents/inbox">No leído</a></li>
                    </ul>
                  </div>
                :
                  null
              }
              <div className={`${this.state.user !== null ? 'col-md-9' : 'col-md-12'}`}>
                <div className="panel">
                  <DocumentList documents={this.state.documents} isDocumentsBox={true} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DocumentsBox;
