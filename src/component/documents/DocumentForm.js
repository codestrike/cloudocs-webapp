import React, { Component } from 'react';
import request from 'reqwest';
import AuthenticationStore from '../../store/AuthenticationStore';

class DocumentForm extends Component {
  constructor() {
    super();

    this.state = {
      user: AuthenticationStore.getUser(),
      destinations: []
    };

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    request({
      url: 'http://40.71.192.54:9000/api-cloudocs/users/getusers',
      method: "GET",
      crossOrigin: true,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      this.setState({
        destinations: response
      });
    })
  }

  onChange(event) {
    let data = {
      name: event.target.name,
      value: event.target.value
    }

    if (event.target.value.trim !== "") {
      this.props.onFillForm.call(null, data);
    }
  }

  render() {
    return (
      <div className="DocumentForm">
        <form className="form-horizontal">
          <div className="col-md-12" style={{marginBottom: '15px'}}>
            <h4>Enviar documento</h4>
            <p className="helper-text">Ingrese el asunto del envío y el destinatario</p>
          </div>
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label col-md-4">Asunto</label>
              <div className="col-md-6">
                <input type="text" name="subject" className="form-control" onChange={this.onChange} />
              </div>
            </div>
            <div className="form-group">
              <label className="control-label col-md-4">Destinatario</label>
              <div className="col-md-6">
                <select className="form-control" name="destinationCode" onChange={this.onChange}>
                  <option value="">Seleccione</option>
                  {
                    this.state.destinations.map((destination) => {
                      let option;
                      if (destination.id !== this.state.user.id) {
                        option = <option>{destination.name} {destination.lastName} ({destination.organization.shortName})</option>
                      }

                      return option;
                    })
                  }
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default DocumentForm;
