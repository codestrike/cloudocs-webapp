import React, { Component } from 'react';
import checkedIcon from '../../icons/checked.png';

class TemplateList extends Component {

  onSelectTemplate(id) {
    this.props.onSelectTemplate.call(null, id);
  }

  render() {
    return (
      <div className="TemplateList">
        {
          this.props.templates.map((template) => {
            let className = `CreateDocumentForm__template ${this.props.selectedTemplate !== null && template.id === this.props.selectedTemplate.id ? 'selected' : ''}`;
            return (
              <div className="col-md-3" key={template.id}>
                <div className={className} onClick={() => this.onSelectTemplate(template.id)}>
                  <span>{template.name}</span>
                  {
                    this.props.selectedTemplate !== null && template.id === this.props.selectedTemplate.id ?
                      <img src={checkedIcon} alt="Checked" />
                    :
                      null
                  }
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default TemplateList;
