const subdomain = require('express-subdomain');
const path = require('path');
const express = require('express');
const http = require('http');

const app = express();
const router = express.Router();

app.use(express.static(path.resolve(__dirname, '..', 'build')));
app.use(subdomain('*', router));

router.get('*', (req, res) => {
  var subdomain = req.hostname.split(".")[0];
  res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});

module.exports = app;
